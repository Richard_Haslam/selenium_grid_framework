﻿using FrameworkExample.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace FrameworkExample.StepDefinitions
{
    [Binding]
    public sealed class ExampleSteps
    {
        public IWebDriver Driver;
        public DuckDuckGoSearchPage SearchPage;


        [Given("I have opened '(.*)'")]
        public void IHaveOpened(string url)
        {
            Driver = WebDrivers.New(BrowserTypes.Chrome);
            SearchPage = new DuckDuckGoSearchPage(Driver);

            SearchPage.Open();
        }

        [When("I search for '(.*)'")]
        public void ISearchFor(string searchTerm)
        {
            SearchPage
                 .EnterSearchTerm(searchTerm)
                 .ClickSearchButton();
        }

        [Then("I will see results about '(.*)'")]
        public void IWillSeeResultsAbout(string searchTerm)
        {
            Assert.True(SearchPage.SearchResults.Count > 0);

            SearchPage
                 .Menu().Open()
                 .Select().AboutUs();

            Driver.Close();
            Driver.Quit();
        }
    }
}