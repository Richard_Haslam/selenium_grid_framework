using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using System;
using OpenQA.Selenium.Remote;
using System.Reflection;

namespace FrameworkExample
{
    public enum BrowserTypes
    {
        Chrome,
        Edge,
        Firefox
    }

    public static class WebDrivers
    {
        public static BrowserTypes DefaultBrowserType {get; set;} = BrowserTypes.Chrome;

        public static IWebDriver New(BrowserTypes browser)
        {
            switch(browser)
            {
                default:          
                case BrowserTypes.Chrome : return ChromeDriver();
                case BrowserTypes.Edge : return EdgeDriver(); 
                case BrowserTypes.Firefox : return FirefoxDriver();
            }
        }

        private static IWebDriver ChromeDriver()
        {
            var options = new ChromeOptions();
            options.AcceptInsecureCertificates = true;

            #if (!DEBUG)
                var chrome = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options);
            #else
                var chrome = new ChromeDriver(options);
            #endif

            return SetupDriver(chrome);
        }

        private static IWebDriver EdgeDriver()
        {
            var options = new EdgeOptions();
            options.AcceptInsecureCertificates = true;

            var edge = new EdgeDriver(options);

            return SetupDriver(edge);
        }

        private static IWebDriver FirefoxDriver()
        {
            var options = new FirefoxOptions();
            options.AcceptInsecureCertificates = true;

            #if GRID            
                var firefox = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options);
            #else
                var firefox = new FirefoxDriver(options);
            #endif

            return SetupDriver(firefox);
        }

        private static IWebDriver SetupDriver(RemoteWebDriver webdriver)
        {
            webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            webdriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);
            return webdriver;
        }    
    }
}