using OpenQA.Selenium;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FrameworkExample.PageObjects
{
    public class DuckDuckGoSearchPage
    {
        public ReadOnlyCollection<ISearchResult> SearchResults;
        private List<ISearchResult> _SearchResults;
        private IWebDriver Driver;        
        private readonly By SeachText = By.Id("search_form_input_homepage");
        private readonly By SearchButton = By.Id("search_button_homepage");
        private readonly By SearchResult = By.ClassName("result__body");
        private readonly By ResultsContainer = By.ClassName("results--main");
        private readonly By MenuButton = By.ClassName("header__button--menu");
                
        public DuckDuckGoSearchPage(IWebDriver driver)
        {
            Driver = driver;    
            _SearchResults = new List<ISearchResult>();    
            SearchResults = new ReadOnlyCollection<ISearchResult>(_SearchResults);    
        }

        public string Url => "https://start.duckduckgo.com";        

        public DuckDuckGoSearchPage Open()
        {
            Driver.Navigate().GoToUrl(Url);
            return this;
        } 

        public DuckDuckGoSearchPage EnterSearchTerm(string term)
        {
            Driver.FindElement(SeachText).SendKeys(term);
            return this;
        }

        public DuckDuckGoSearchPage ClickSearchButton()
        {
            Driver.FindElement(SearchButton).Click();
            UpdateSerachResults();        
            return this;
        }

        public DuckDuckGoMenu Menu()
            => new DuckDuckGoMenu(Driver);        

        private void UpdateSerachResults()
        {
            _SearchResults.Clear();
            WaitForSearchResults();
            foreach(var result in Driver.FindElements(SearchResult))
            {
                _SearchResults.Add(new DuckDuckGoSearchResult(result));
            }
        }

        private void WaitForSearchResults() => Driver.FindElement(ResultsContainer);
    }
}