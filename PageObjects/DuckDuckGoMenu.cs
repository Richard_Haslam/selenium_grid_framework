using OpenQA.Selenium;

namespace FrameworkExample.PageObjects
{
    public class DuckDuckGoMenu
    {
        private IWebDriver Driver;
        private readonly By CloseButton = By.ClassName("nav-menu__close");
        private readonly By OpenButton = By.LinkText("⇶");

        public DuckDuckGoMenu(IWebDriver driver)
        {
            Driver = driver;
        }

        public DuckDuckGoMenu Open()
        {
            Driver.FindElement(OpenButton).Click();
            return this;
        }

        public void Close()
        {
            Driver.FindElement(CloseButton).Click();
        }

        public DuckDuckGoMenuOptions Select()
        {
            return new DuckDuckGoMenuOptions(Driver);
        }     
    }
}