using OpenQA.Selenium;

namespace FrameworkExample.PageObjects
{
    public class DuckDuckGoSearchResult : ISearchResult
    {
        private IWebElement ContainerElement;
        private By _Title = By.ClassName("result__body links_main links_deep");
        private By _Address = By.ClassName("result__extras__url");
        private By _Description = By.ClassName("result__snippet js-result-snippet");

        public DuckDuckGoSearchResult(IWebElement containerElement)
        {   
            ContainerElement = containerElement;
        }

        public string Title => ContainerElement.FindElement(_Title).Text;
        public string Address => ContainerElement.FindElement(_Address).Text;
        public string Description => ContainerElement.FindElement(_Description).Text;
    }
}