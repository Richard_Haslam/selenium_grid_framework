namespace FrameworkExample.PageObjects
{
    public interface ISearchResult
    {
        string Title { get; }
        string Address { get; }
        string Description { get; }
    }
}