using OpenQA.Selenium;

namespace FrameworkExample.PageObjects
{
    public class DuckDuckGoMenuOptions : IMenuOptions
    {
        private IWebDriver Driver;

        public DuckDuckGoMenuOptions(IWebDriver driver)
            => Driver = driver;

        public void PrivateSearch() 
            => ClickLink("Private Search");
        public void AppAndExtention() 
            => ClickLink("App and Extension");
        public void PrivacyBlog() 
            => ClickLink("Privacy Blog");

        public void HelpSpreadPrivacy() 
            => ClickLink("Help Spread Privacy");

        public void AboutUs() 
            => ClickLink("About Us");

        public void PrivacyPolicy() 
            => ClickLink("Privacy Policy");


        private void ClickLink(string linkText) 
            => Driver.FindElement(By.LinkText(linkText)).Click();
    }
}