﻿Feature: Example
    As a beer lover
    I want to find out more about beer  
    So that I can enjoy the finest beers

    Scenario: Using Google
        Given I have opened 'http://www.google.co.uk'
        When I search for 'beer'
        Then I will see results about 'beer'